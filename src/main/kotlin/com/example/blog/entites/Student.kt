package com.example.blog.entites

import com.example.blog.entites.*
import jakarta.persistence.*


@Entity
@Table(name = "student")
data class Student(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//because of IDENTITY WE HAVE TO SPECIFY NULL FOR ID
    @Column(name = "student_id")
    val id: Int? = null,
    val name: String,
    @Column(name = "enrolment_status")
    val enrollStatus: String,
    val age: Int,
    @Column(name = "current_student")
    val currentStatus: Boolean,
    @OneToMany(mappedBy = "student", cascade = [CascadeType.ALL], orphanRemoval = true)
    val subjectResults: MutableList<SubjectResults> = mutableListOf(),
    @ManyToMany(mappedBy = "students")
    val faculties: List<Faculty> = mutableListOf()
)