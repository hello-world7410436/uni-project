package com.example.blog.controllers

import com.example.blog.dto.SubjectResultsDTO
import com.example.blog.entites.SubjectResults
import com.example.blog.services.MarksServices
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.sql.SQLException

@RestController
@RequestMapping("/marks")
class MarksController(val marksServices: MarksServices) {

    @PostMapping("/create")
    fun createMark(
        @RequestBody newMarkData: SubjectResultsDTO
    ): ResponseEntity<SubjectResultsDTO> {

        val newMark = marksServices.createNewMark(
            SubjectResultsDTO(
                resultId = null,
                studentId = newMarkData.studentId,
                subjectId = newMarkData.subjectId,
                marks = newMarkData.marks,
                sessionId = newMarkData.sessionId
            )
        )
        return if (newMark != null) {
            ResponseEntity.ok(newMark)
        } else {
            ResponseEntity.notFound().build()
        }
    }

    @DeleteMapping("/delete/{studentId}/{subjectId}/{sessionId}")
    fun deleteResults(
        @PathVariable studentId: Int,
        @PathVariable subjectId: Int,
        @PathVariable sessionId: Int
    ): ResponseEntity<String> {
        val deleteMark = marksServices.deleteMark(studentId, subjectId, sessionId)
        marksServices.deleteMark(studentId, subjectId, sessionId)

        if (deleteMark) {
            return ResponseEntity.ok("Mark deleted successfully")
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error deleting mark")
    }
}