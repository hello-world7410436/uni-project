package com.example.blog.dto

data class SubjectResultsDTO(
    val resultId: Int? = null,
    val studentId: Int,
    val subjectId: Int,
    var marks: String,
    val sessionId: Int,
)