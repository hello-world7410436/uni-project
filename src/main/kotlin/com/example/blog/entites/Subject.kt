package com.example.blog.entites

import jakarta.persistence.*

@Entity
data class Subject(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subject_id")
    val subjectId: Int,
    val name: String,
    @Column(name = "level_code")
    val subjectLevel: Int,
    val campus: String,
    @OneToMany(mappedBy = "subject")
    val subjectResults: MutableList<SubjectResults> = mutableListOf()
)
