import React from "react";
import {
  NavLink,
  useLoaderData,
  useParams,
  useRouteLoaderData,
} from "react-router-dom";

const Transcript = () => {
  const { results, faculty } = useLoaderData();
  const studentData = useRouteLoaderData("student-details");
  const params = useParams();
  console.log(results);
  return (
    <div className="transcript-container">
      <div className="transcript">
        <h1>Student Transcript</h1>
        <div className="student-info">
          <p>
            <strong>Student Name: </strong>
            {studentData.name}
          </p>
          <p>
            <strong>Faculty Name:</strong> {faculty}
          </p>
        </div>
        <table className="marks-table">
          <thead>
            <tr>
              <th>Subject</th>
              <th>Mark</th>
              <th>Session</th>
              <th>Campus</th>
            </tr>
          </thead>
          <tbody>
            {results.map((s) => (
              <tr key={s.subjectResult.resultId}>
                <td>{s.subject.name}</td>
                <td>{s.subjectResult.marks.toUpperCase()}</td>
                <td>{s.subjectResult.sessionId}</td>
                <td>{s.subject.campus}</td>
              </tr>
            ))}
          </tbody>
        </table>
        <div className="mark-back-btn">
          <NavLink to={`../student/${params.studentId}`} className="nav-link">
            <button className="button back">Back</button>
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Transcript;
