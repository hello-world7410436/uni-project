package com.example.blog.dto

data class FacultyDTO(
    val facultyId: Int,
    val facultyName: String,
    val campus: String,
)