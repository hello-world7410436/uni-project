package com.example.blog.repositories

import com.example.blog.entites.Subject
import org.springframework.data.jpa.repository.JpaRepository

interface SubjectRepository : JpaRepository<Subject, Int>