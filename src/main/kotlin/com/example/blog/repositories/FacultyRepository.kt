package com.example.blog.repositories

import com.example.blog.entites.Faculty
import org.springframework.data.jpa.repository.JpaRepository

interface FacultyRepository : JpaRepository<Faculty,Int>
