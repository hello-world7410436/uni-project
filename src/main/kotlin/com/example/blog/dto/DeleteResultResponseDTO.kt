package com.example.blog.dto

data class DeleteResultResponseDTO(
    val success: Boolean,
    val message: String
)

