package com.example.blog.repositories

import com.example.blog.entites.Student
import org.springframework.data.jpa.repository.JpaRepository

interface StudentRepository : JpaRepository<Student, Int>