import { Form, NavLink, useParams } from "react-router-dom";

function AddMark() {
  const params = useParams();
  console.log(params.studentId);

  const subjects = [
    { name: "Introduction to Programming", value: "1" },
    { name: "Logic", value: "2" },
    { name: "Data Structures", value: "3" },
    { name: "Object Oriented Programming", value: "4" },
    { name: "Databases", value: "5" },
    { name: "Analysis of Digital Art and Media", value: "6" },
    { name: "Interaction Design", value: "7" },
    { name: "Differential Calculus", value: "8" },
    { name: "Linear Algebra", value: "9" },
    { name: "Discrete Mathematics", value: "10" },
    { name: "Formal Languages and Logic", value: "11" },
  ];

  return (
    <div className="add-mark-container">
      <h2 className="add-mark-heading">Add Student Marks</h2>
      <Form method="POST" className="add-mark-form">
        <input type="hidden" name="studentId" value={params.studentId} />
        <label className="add-mark-label">
          <p>Subject:</p>
          <select className="add-mark-input subject" name="subject" required>
            {subjects.map((subject) => (
              <option key={subject.value} value={subject.value}>
                {subject.name}
              </option>
            ))}
          </select>
        </label>
        <br />
        <label className="add-mark-label">
          <p>Mark:</p>
          <select className="add-mark-input mark" name="mark" required>
            <option value="hd">HD</option>
            <option value="d">D</option>
            <option value="c">C</option>
            <option value="p">P</option>
            <option value="f">F</option>
          </select>
        </label>
        <br />
        <label className="add-mark-label">
          <p>Session ID:</p>
          <select
            className="add-mark-input sessionId"
            name="sessionId"
            required
          >
            <option value="1">Session 1</option>
            <option value="2">Session 2</option>
            <option value="3">Session 3</option>
            <option value="4">Session 4</option>
          </select>
        </label>
        <br />
        <div>
          <button type="submit" className="add-mark-button">
            Add Mark
          </button>
        </div>
      </Form>
      <div className="mark-back-btn">
        <NavLink to={`../student/${params.studentId}`} className="nav-link">
          <button className="button back">Back</button>
        </NavLink>
      </div>
    </div>
  );
}

export default AddMark;
