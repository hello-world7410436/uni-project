package com.example.blog.services

import com.example.blog.dto.*
import com.example.blog.entites.SubjectResults
import com.example.blog.repositories.*
import org.springframework.stereotype.Service

@Service
class StudentServices(
    val studentRepository: StudentRepository,
        val subjectRepository: SubjectRepository,
    val studentFacultyRepository: StudentFacultyRepository,
    val facultyRepository: FacultyRepository,
) {


    fun getStudentById(id: Int): StudentDTO? {
        val student = studentRepository.findById(id)
        if (student.isPresent) {
            println("Student found: ${student.get().name}")
            return StudentDTO(
                age = student.get().age,
                name = student.get().name,
                enrollStatus = student.get().enrollStatus,
                currentStatus = student.get().currentStatus
            )
        } else {
            println("Student not found for ID: $id")
            return null
        }
    }

    fun getTranscriptById(id: Int): TranscriptDTO? {
        val student = studentRepository.findById(id)

        return if (student.isPresent) {
            val transcriptEntries = mutableListOf<TranscriptEntry>()
            val results = student.get().subjectResults
            val faculty = studentFacultyRepository.findByStudentId(id)
            val facultyName = facultyRepository.findById(faculty!!.facultyId).get().facultyName

            for (result in results) {
                val subject = subjectRepository.findById(result.subject.subjectId).get()
                val subjectDTO = SubjectDTO(
                    subjectId = subject.subjectId,
                    name = subject.name,
                    subjectLevel = subject.subjectLevel,
                    campus = subject.campus
                )
                val subjectResultsDTO = SubjectResultsDTO(

                    resultId = result.resultId!!,
                    studentId = result.student.id!!,
                    subjectId = result.subject.subjectId,
                    marks = result.marks,
                    sessionId = result.session.sessionId
                )
                val transcriptEntry = TranscriptEntry(subjectDTO, subjectResultsDTO)
                transcriptEntries.add(transcriptEntry)
            }
            return TranscriptDTO(transcriptEntries, facultyName)

        } else {
            null
        }
    }

}