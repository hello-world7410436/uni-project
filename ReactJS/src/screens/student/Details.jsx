import { useLoaderData, useParams, useRouteLoaderData } from "react-router-dom";
import BackButton from "../../button/Backbutton";
import avatar from "../../img/student.svg";

const Details = () => {
  const params = useParams();
  const student = useRouteLoaderData("student-details");

  return (
    <div className="details-container">
      <div className="profile">
        <img src={avatar} alt="Student Avatar" className="student-avatar" />
      </div>
      <h1 className="student-name">{student.name}</h1>
      <div className="student-info" key={params.studentId}>
        <div className="info-row">ID: {params.studentId}</div>
        <div className="info-row">Age: {student.age}</div>
        <div className="info-row">Enrolment: {student.enrollStatus}</div>
      </div>
      <div className="display-back-btn">
        <BackButton />
      </div>
    </div>
  );
};

export default Details;
