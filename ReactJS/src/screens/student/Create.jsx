import { Form } from "react-router-dom";
import BackButton from "../../button/Backbutton";
import "./../../Create.css";

const Create = () => {
  // Form method="post"
  return (
    <div className="create-container">
      <h2>Create Student</h2>
      <Form method="POST" className="create-form">
        <div className="form-group">
          <label htmlFor="firstName">First Name</label>
          <input type="text" id="firstName" name="firstName" />
        </div>
        <div className="form-group">
          <label htmlFor="lastName">Last Name</label>
          <input type="text" id="lastName" name="lastName" />
        </div>
        <div className="form-group">
          <label htmlFor="age">Age</label>
          <input type="number" id="age" name="age" min="16" />
        </div>
        <div className="form-group">
          <label htmlFor="enrollStatus">Enrollment Status:</label>
          <select id="enrollStatus" name="enrollStatus">
            <option value="full-time">Full-Time</option>
            <option value="part-time">Part-Time</option>
          </select>
          <br></br>
        </div>
        <button className="submit-button">Submit</button>{" "}
      </Form>
      <BackButton />
    </div>
  );
};

export default Create;
