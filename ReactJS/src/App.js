import { useParams, useNavigate, NavLink } from "react-router-dom";
import { useState } from "react";
import "./App.css";

function App() {
  const { studentId } = useParams();
  const navigate = useNavigate();
  const [searchValue, setSearchValue] = useState(studentId || "");

  const handleSearch = async () => {
    navigate(`/student/${parseInt(searchValue)}`);
  };

  return (
    <div className="App">
      <div>
        <h1>Student Search</h1>
        <div className="student-search-container">
          <input
            type="number"
            placeholder="Enter Student ID"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
          />
          <button
            disabled={!searchValue || isNaN(parseInt(searchValue))}
            onClick={() => handleSearch(studentId)}
            className="search-btn"
          >
            Search
          </button>
        </div>
      </div>
      <div className="create-container">
        <NavLink to="/create" className="nav-link">
          <button className="button">Create New Student</button>
        </NavLink>
      </div>
    </div>
  );
}

export default App;
