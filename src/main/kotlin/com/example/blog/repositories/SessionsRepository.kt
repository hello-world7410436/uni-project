package com.example.blog.repositories

import com.example.blog.entites.Sessions
import org.springframework.data.jpa.repository.JpaRepository

interface SessionsRepository : JpaRepository<Sessions, Int>