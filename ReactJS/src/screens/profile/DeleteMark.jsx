import { Form, NavLink, useParams } from "react-router-dom";
import "./DeleteMarkPage.css";

function DeleteMark() {
  const params = useParams();
  const subjects = [
    { name: "Introduction to Programming", value: "1" },
    { name: "Logic", value: "2" },
    { name: "Data Structures", value: "3" },
    { name: "Object Oriented Programming", value: "4" },
    { name: "Databases", value: "5" },
    { name: "Analysis of Digital Art and Media", value: "6" },
    { name: "Interaction Design", value: "7" },
    { name: "Differential Calculus", value: "8" },
    { name: "Linear Algebra", value: "9" },
    { name: "Discrete Mathematics", value: "10" },
    { name: "Formal Languages and Logic", value: "11" },
  ];
  return (
    <div className="delete-mark-container">
      <h2>Delete Mark</h2>
      <Form method="DELETE" className="delete-mark-form">
        <div className="delete-form-group">
          <input type="hidden" name="studentId" value={params.studentId} />
          <label className="delete-add-mark-label">
            <p>Subject:</p>
            <select className="delete-add-mark-input" name="subjectId" required>
              {subjects.map((subject) => (
                <option key={subject.value} value={subject.value}>
                  {subject.name}
                </option>
              ))}
            </select>
          </label>
        </div>
        <div className="delete-form-group">
          <label htmlFor="sessionId">Session ID:</label>
          <input type="number" id="sessionId" name="sessionId" required />
        </div>
        <div className="delete-btn-container">
          <button className="button" type="submit">
            Delete Mark
          </button>
        </div>
      </Form>
      <div className="mark-back-btn">
        <NavLink to={`../student/${params.studentId}`} className="nav-link">
          <button className="button back">Back</button>
        </NavLink>
      </div>
    </div>
  );
}

export default DeleteMark;
