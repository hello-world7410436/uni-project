package com.example.blog.dto

data class StudentDTO(
    val name: String,
    val enrollStatus: String,
    val age: Int,
    val currentStatus: Boolean,
)
