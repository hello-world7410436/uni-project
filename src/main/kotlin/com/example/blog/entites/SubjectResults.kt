package com.example.blog.entites


import jakarta.persistence.*

@Entity
data class SubjectResults(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "result_id")
    val resultId: Int? =null,
    @ManyToOne(fetch = FetchType.LAZY)//check this eager,lazy
    @JoinColumn(name = "student_id")
    val student:Student,
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subject_id")
    val subject: Subject,
    var marks: String,
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "session_id")
    val session: Sessions,
)