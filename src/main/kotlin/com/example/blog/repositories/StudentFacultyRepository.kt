package com.example.blog.repositories
import com.example.blog.entites.StudentFaculty
import org.springframework.data.jpa.repository.JpaRepository

interface StudentFacultyRepository : JpaRepository<StudentFaculty, Int> {
    fun findByStudentId(studentId: Int): StudentFaculty?
}