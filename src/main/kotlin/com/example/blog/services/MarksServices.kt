package com.example.blog.services

import com.example.blog.dto.SubjectResultsDTO
import com.example.blog.entites.SubjectResults
import com.example.blog.repositories.SessionsRepository
import com.example.blog.repositories.StudentRepository
import com.example.blog.repositories.SubjectRepository
import com.example.blog.repositories.SubjectResultsRepository
import org.springframework.stereotype.Service
import java.sql.SQLException

@Service
class MarksServices(
    val subjectResultsRepository: SubjectResultsRepository,
    val studentRepository: StudentRepository,
    val subjectRepository: SubjectRepository,
    val sessionsRepository: SessionsRepository
) {

    fun createNewMark(subjectResultsDTO: SubjectResultsDTO): SubjectResultsDTO {
        val markExists =
            subjectResultsRepository.findSubjectByStudentAndSubjectAndSession(
                student = studentRepository.findById(subjectResultsDTO.studentId).orElseThrow(),
                subject = subjectRepository.findById(subjectResultsDTO.subjectId).orElseThrow(),
                session = sessionsRepository.findById(subjectResultsDTO.sessionId).orElseThrow()!!,
            )

        if (markExists == null) {
            val student = studentRepository.findById(subjectResultsDTO.studentId).orElseThrow()
            val subject = subjectRepository.findById(subjectResultsDTO.subjectId).orElseThrow()
            val session = sessionsRepository.findById(subjectResultsDTO.sessionId).orElseThrow()

            val markEntity =
                SubjectResults(
                    null,
                    student,
                    subject,
                    subjectResultsDTO.marks,
                    session
                )

            subjectResultsRepository.save(markEntity)
            return markEntity.let {
                SubjectResultsDTO(it.resultId, it.student.id!!, it.subject.subjectId, it.marks, it.session.sessionId)
            }

        } else {
            markExists.marks = subjectResultsDTO.marks
            val updateMark = subjectResultsRepository.save(markExists)
            return SubjectResultsDTO(
                updateMark.resultId,
                updateMark.student.id!!,
                updateMark.subject.subjectId,
                updateMark.marks,
                updateMark.session.sessionId
            )

        }

    }

    fun deleteMark(studentId: Int, subjectId: Int, sessionId: Int): Boolean {
        val student = studentRepository.findById(studentId).orElseThrow()
        val subject = subjectRepository.findById(subjectId).orElseThrow()
        val session = sessionsRepository.findById(sessionId).orElseThrow()
        val findMark = subjectResultsRepository.findSubjectByStudentAndSubjectAndSession(student, subject, session)
        return if (findMark != null) {
            subjectResultsRepository.delete(findMark)
            true
        } else {
            false
        }
    }


}