package com.example.blog.entites

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import java.util.*

@Entity
data class StudentFaculty(
    @Id
    @Column(name = "student_id")
    val studentId: Int,
    @Column(name = "faculty_id")
    val facultyId: Int,
    @Column(name = "join_date")
    val joiningDate: Date
)