package com.example.blog.controllers

import com.example.blog.dto.StudentDTO
import com.example.blog.dto.SubjectResultsDTO
import com.example.blog.dto.TranscriptDTO
import com.example.blog.services.StudentServices
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/student")
class StudentController(
    val studentServices: StudentServices,

    ) {
    @GetMapping("/{id}")
    fun getInfoById(@PathVariable id: Int): ResponseEntity<StudentDTO> {

        val student = studentServices.getStudentById(id)
        return if (student != null) {
            ResponseEntity.ok(student)
        } else {
            //returns 404 not found
            ResponseEntity.notFound().build()
        }
    }


    @GetMapping("/{id}/transcript")
    fun getTranscript(@PathVariable id: Int): ResponseEntity<TranscriptDTO> {
        val transcript = studentServices.getTranscriptById(id)
        return if (transcript != null) {
            ResponseEntity.ok(transcript)
        } else {
            //returns 404 not found
            ResponseEntity.notFound().build()
        }


    }}
