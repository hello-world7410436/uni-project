import { Outlet, NavLink, useParams } from "react-router-dom";

const HomePage = () => {
  const params = useParams();
  return (
    <div className="Home">
      <div>
        <h1>Profile Page</h1>
        <div className="button-container">
          <NavLink to={`${params.studentId}/addmark`} className="nav-link">
            <button className="button">Marks</button>
          </NavLink>

          <NavLink to={`/${params.studentId}/transcript`} className="nav-link">
            <button className="button">Transcript</button>
          </NavLink>

          <NavLink to={`/${params.studentId}/deletemark`} className="nav-link">
            <button className="button">Delete Mark</button>
          </NavLink>
        </div>
      </div>
      <div>
        <Outlet />
      </div>
    </div>
  );
};

export default HomePage;
