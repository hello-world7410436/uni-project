package com.example.blog.entites

import jakarta.persistence.*

@Entity
data class Faculty(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "faculty_id")
    val facultyId: Int,
    @Column(name = "faculty_name")
    val facultyName: String,
    val campus: String,
    @ManyToMany
    @JoinTable(
        name = "student_faculty",
        joinColumns = [JoinColumn(name = "faculty_id")],
        inverseJoinColumns = [JoinColumn(name = "student_id")]
    )
    val students: List<Student> = mutableListOf())