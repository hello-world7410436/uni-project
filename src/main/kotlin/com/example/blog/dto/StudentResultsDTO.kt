package com.example.blog.dto

data class StudentResultsDTO(
    val studentId: Int,
    val subjectId: Int,
    val session: Int,
    val mark: String,
);
