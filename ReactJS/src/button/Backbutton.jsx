import { NavLink } from "react-router-dom";

const BackButton = () => {
  return (
    <NavLink to=".." className="nav-link">
      <button className="button back">Back</button>
    </NavLink>
  );
};

export default BackButton;
