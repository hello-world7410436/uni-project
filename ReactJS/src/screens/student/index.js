import Create from "./Create";
import Details from "./Details";
import Update from "./Update";
import HomePage from "./HomePage";
import StudentCreated from "./StudentCreated";
import ErrorPage from "./ErrorPage";

const Student = {
  Create,
  Details,
  Update,
  HomePage,
  StudentCreated,
  ErrorPage,
};
export default Student;
