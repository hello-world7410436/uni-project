package com.example.blog.services

import com.example.blog.entites.Student
import com.example.blog.dto.StudentDTO
import com.example.blog.repositories.StudentFacultyRepository
import com.example.blog.repositories.StudentRepository
import com.example.blog.repositories.SubjectResultsRepository
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class EnrolmentServices(
    val studentRepository: StudentRepository,

) {

    fun createStudent(studentDTO: StudentDTO): StudentDTO {
        //converting studentDTO to StudentEntity
        val studentEntity = studentDTO.let {
            Student(age = it.age, name = it.name, enrollStatus = it.enrollStatus, currentStatus = it.currentStatus)
        }
        //saving data
        studentRepository.save(studentEntity)

        return studentEntity.let {
            StudentDTO(age = it.age, name = it.name, enrollStatus = it.enrollStatus, currentStatus = it.currentStatus)
        }
    }

    fun deleteStudent(id: Int): ResponseEntity<Unit> {
        val student = studentRepository.findById(id)
        return if (student.isPresent) {
            studentRepository.deleteById(id)
            ResponseEntity.noContent().build()//return 204 No content to confirm del.
        } else {
            ResponseEntity.notFound().build()// return 404 Not found error
        }
    }


}