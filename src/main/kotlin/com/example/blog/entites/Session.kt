package com.example.blog.entites
import jakarta.persistence.*

@Entity
data class Sessions(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
      @Column(name = "session_id")
    val sessionId: Int,
    val semester: String,
    val year: Int,

    @OneToMany(mappedBy = "session")
val subjectResults: List<SubjectResults> = mutableListOf()
)