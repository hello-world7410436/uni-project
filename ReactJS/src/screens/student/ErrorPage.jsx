import React from "react";
import BackButton from "../../button/Backbutton";

const ErrorPage = () => {
  return (
    <div className="error-page">
      <div className="error-container">
        <h1 className="error-message">Student Hasn't Been Found</h1>
        <p>Please Check Student ID And Try Again!</p>
      </div>
      <BackButton />
    </div>
  );
};

export default ErrorPage;
