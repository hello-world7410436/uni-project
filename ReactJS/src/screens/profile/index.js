import Transcript from "./Transcript";
import AddMark from "./AddMark";
import DeleteMarkPage from "./DeleteMark";

const Profile = { Transcript, AddMark, DeleteMarkPage };
export default Profile;
