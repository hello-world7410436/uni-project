import { json, redirect } from "react-router-dom";

export async function fetchData(url) {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error("Something went wrong");
  } else {
    const data = await response.json();

    return data;
  }
}

export async function loadStudent(params) {
  return fetchData(`http://localhost:8080/student/${parseInt(params)}`);
}
export async function loadTranscript(params) {
  return fetchData(
    `http://localhost:8080/student/${parseInt(params)}/transcript`
  );
}

export const action = async ({ request, params }) => {
  const data = await request.formData();

  const studentData = {
    age: data.get("age"),
    name: data.get("firstName") + " " + data.get("lastName"),
    enrollStatus: data.get("enrollStatus"),
    currentStatus: true,
  };

  const response = await fetch(`http://localhost:8080/enrolment/enroll`, {
    method: `POST`,
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(studentData),
  });

  if (!response.ok) {
    throw json({ message: "could not save Student" }, { status: 500 });
  }

  return redirect(`/studentcreated`);
};

export const addMarkAction = async ({ request, params }) => {
  const data = await request.formData();
  const studentId = data.get("studentId");
  const mark = data.get("mark");

  const newMarkData = {
    studentId: studentId,
    subjectId: parseInt(data.get("subject")),
    marks: mark,
    sessionId: parseInt(data.get("sessionId")),
  };

  const response = await fetch(`http://localhost:8080/marks/create`, {
    method: `POST`,
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(newMarkData),
  });

  if (!response.ok) {
    throw json({ message: "could not save Mark" }, { status: 500 });
  }

  return redirect(`/student/${studentId}`);
};

export const deleteMarkAction = async ({ request, params }) => {
  const data = await request.formData();
  const studentId = parseInt(data.get("studentId"));
  const subjectId = parseInt(data.get("subjectId"));
  const sessionId = parseInt(data.get("sessionId"));

  const response = await fetch(
    `http://localhost:8080/marks/delete/${studentId}/${subjectId}/${sessionId}`,
    {
      method: request.method,
      headers: { "Content-Type": "application/json" },
    }
  );

  if (!response.ok) {
    throw json({ message: "could not delete Mark" }, { status: 500 });
  }

  return redirect(`/student/${studentId}`);
};
