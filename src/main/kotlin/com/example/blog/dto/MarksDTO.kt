package com.example.blog.dto

data class MarksDTO(val subject: String, val mark: String, val session: String)

