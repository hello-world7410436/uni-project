package com.example.blog.controllers

import com.example.blog.dto.StudentDTO
import com.example.blog.services.EnrolmentServices
import org.springframework.http.*
import org.springframework.stereotype.Controller

import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/enrolment")
class EnrolmentController(
    val enrolmentServices: EnrolmentServices
) {

    @PostMapping("/enroll")
    @ResponseStatus(HttpStatus.CREATED)
    fun createStudent(@RequestBody studentDTO: StudentDTO) = enrolmentServices.createStudent(studentDTO)

    @GetMapping("/unenroll/{id}")
    fun unenroll(@PathVariable id: Int) = enrolmentServices.deleteStudent(id)

}