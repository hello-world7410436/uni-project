package com.example.blog.dto

import com.example.blog.entites.Subject
import com.example.blog.entites.SubjectResults

data class TranscriptDTO(
    val results: List<TranscriptEntry>,
    val faculty: String,

)
data class TranscriptEntry(
    val subject: SubjectDTO,
    val subjectResult: SubjectResultsDTO
)