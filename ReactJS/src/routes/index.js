import { createBrowserRouter } from "react-router-dom";
import App from "../App";
import {
  loadStudent,
  action as createStudentAction,
  loadTranscript,
  addMarkAction,
  deleteMarkAction,
} from "../web-services";
import Student from "../screens/student";
import Profile from "../screens/profile";
const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/",
    element: <Student.HomePage />,
    id: "student-details",
    loader: ({ request, params }) => loadStudent(params.studentId),
    errorElement: <Student.ErrorPage />,
    children: [
      {
        path: "student/:studentId",
        element: <Student.Details />,
        errorElement: <Student.ErrorPage />,
      },
      {
        path: ":studentId/transcript",
        element: <Profile.Transcript />,
        errorElement: <Student.ErrorPage />,
        loader: ({ request, params }) => loadTranscript(params.studentId),
      },
      {
        path: ":studentId/addmark",
        element: <Profile.AddMark />,
        errorElement: <Student.ErrorPage />,
        action: addMarkAction,
      },
      {
        path: ":studentId/deletemark",
        element: <Profile.DeleteMarkPage />,
        action: deleteMarkAction,
      },
    ],
  },

  {
    path: "/create",
    element: <Student.Create />,
    action: createStudentAction,
  },
  {
    path: "/studentcreated",
    element: <Student.StudentCreated />,
  },
]);
export default router;
