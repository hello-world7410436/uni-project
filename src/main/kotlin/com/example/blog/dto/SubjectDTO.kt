package com.example.blog.dto

import com.example.blog.entites.SubjectResults
import jakarta.persistence.Column
import jakarta.persistence.OneToMany

data class SubjectDTO(
    val subjectId: Int,
    val name: String,
    val subjectLevel: Int,
    val campus: String,
)
