package com.example.blog.repositories

import com.example.blog.entites.Sessions
import com.example.blog.entites.Student
import com.example.blog.entites.Subject
import com.example.blog.entites.SubjectResults
import org.hibernate.Session
import org.springframework.data.jpa.repository.JpaRepository

interface SubjectResultsRepository : JpaRepository<SubjectResults, Int> {
    fun findByStudentId(studentId: Int): MutableList<SubjectResults>
    fun findSubjectByStudentAndSubjectAndSession(
        student: Student,
        subject: Subject,
        session: Sessions
    ): SubjectResults?

}