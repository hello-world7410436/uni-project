import React from "react";
import BackButton from "../../button/Backbutton";

const StudentCreated = () => {
  return (
    <div className="page-container">
      <h1 className="success-message">Student Successfully Created</h1>
      <p>Congratulations! The student has been successfully created.</p>
      <BackButton />
    </div>
  );
};

export default StudentCreated;
